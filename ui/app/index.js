import angular from 'angular'
import uirouter from 'angular-ui-router';
import sanitize from 'angular-sanitize';
import ngResource from 'angular-resource'
import ngCookies from 'angular-cookies'

import './common/css/application.css';
import {intercept, stateChangeSuccess, stateChangeStart, routing} from './app.config'
import common from './component/common';
import profile from './component/profile';
import bsTracker from './component/directives/bsHttpRequestTracker';
import homeComponent from './component/home';

const commonModule = angular.module('smlFutbol.common', [ngCookies]);
common(commonModule);

const profileModule = angular.module('smlFutbol.profile', [uirouter, ngResource, ngCookies, 'smlFutbol.common']);
profile(profileModule);

const bsTrackerModule = angular.module('smlFutbol.bsTracker', []);
bsTracker(bsTrackerModule);

const homeModule = angular.module('smlFutbol.home', [uirouter]);
homeComponent(homeModule);

require('./component/main')(angular.module('smlFutbol.main', [uirouter]));

const ngModule = angular.module('smlFutbol',
    [uirouter, sanitize, 'smlFutbol.common', 'smlFutbol.profile', 'smlFutbol.bsTracker', 'smlFutbol.home', 'smlFutbol.main'])
  .config(routing)
  .config(intercept)
  .run(stateChangeStart)
  .run(stateChangeSuccess);
