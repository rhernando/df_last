package desafio.futbol.config

import desafio.futbol.utils.ConfigWithDefault
import com.typesafe.config.Config

trait CoreConfig extends ConfigWithDefault {
  def rootConfig: Config

  lazy val resetLinkPattern = getString("futbol.reset-link-pattern", "http://localhost:8080/#/password-reset?code=%s")
}
