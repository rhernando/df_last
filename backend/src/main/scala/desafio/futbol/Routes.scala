package desafio.futbol

import akka.http.scaladsl.server.Directives._
import desafio.futbol.utils.http.RoutesRequestWrapper
import desafio.futbol.passwordreset.PasswordResetRoutes
import desafio.futbol.user.UsersRoutes
import desafio.futbol.version.VersionRoutes

trait Routes extends RoutesRequestWrapper
    with UsersRoutes
    with PasswordResetRoutes
    with VersionRoutes {

  lazy val routes = requestWrapper {
    pathPrefix("api") {
      passwordResetRoutes ~
        usersRoutes ~
        versionRoutes
    } ~
      getFromResourceDirectory("webapp") ~
      path("") {
        getFromResource("webapp/index.html")
      }
  }
}
