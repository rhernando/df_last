package desafio.futbol.email

case class EmailContentWithSubject(content: String, subject: String)
