package desafio.futbol.sql

import desafio.futbol.utils.ConfigWithDefault
import desafio.futbol.sql.DatabaseConfig._
import com.typesafe.config.Config

trait DatabaseConfig extends ConfigWithDefault {
  def rootConfig: Config

  // format: OFF
  lazy val dbH2Url              = getString(s"futbol.db.h2.properties.url", "jdbc:h2:file:./data/futbol")
  lazy val dbPostgresServerName = getString(PostgresServerNameKey, "")
  lazy val dbPostgresPort       = getString(PostgresPortKey, "5432")
  lazy val dbPostgresDbName     = getString(PostgresDbNameKey, "")
  lazy val dbPostgresUsername   = getString(PostgresUsernameKey, "")
  lazy val dbPostgresPassword   = getString(PostgresPasswordKey, "")
}

object DatabaseConfig {
  val PostgresDSClass       = "futbol.db.postgres.dataSourceClass"
  val PostgresServerNameKey = "futbol.db.postgres.properties.serverName"
  val PostgresPortKey       = "futbol.db.postgres.properties.portNumber"
  val PostgresDbNameKey     = "futbol.db.postgres.properties.databaseName"
  val PostgresUsernameKey   = "futbol.db.postgres.properties.user"
  val PostgresPasswordKey   = "futbol.db.postgres.properties.password"
  // format: ON
}
